import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.parser.PdfTextExtractor;

def pdfFile = new File(basedir, 'target/output.pdf')
assert pdfFile.exists()
assert pdfFile.length() > 0

def extractor = new PdfTextExtractor(new PdfReader(pdfFile.toURI().toURL()))
def text = extractor.getTextFromPage(1).normalize()
assert text.contains('This is my test class, the javadoc should be made into a PDF.')
