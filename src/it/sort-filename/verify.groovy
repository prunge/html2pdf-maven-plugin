import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.parser.PdfTextExtractor;

def pdfFile = new File(basedir, 'target/output.pdf')
assert pdfFile.exists()
assert pdfFile.length() > 0

def extractor = new PdfTextExtractor(new PdfReader(pdfFile.toURI().toURL()))

Page1:
{
	def lines = extractor.getTextFromPage(1).normalize().tokenize('\n')
	assert lines[0] == 'Welcome'
	assert lines[1] == 'This is the first test file.'
}

Page2:
{
	def lines = extractor.getTextFromPage(2).normalize().tokenize('\n')
	assert lines[0] == 'Welcome'
	assert lines[1] == 'This is the second test file.'
}

