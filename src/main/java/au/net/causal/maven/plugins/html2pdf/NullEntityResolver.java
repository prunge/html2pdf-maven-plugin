package au.net.causal.maven.plugins.html2pdf;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * An entity resolver that resolves all entities to zero-length content.
 * <p>
 * 
 * Useful for preventing XML parsers from going to the internet and downloading DTDs and other resources.
 * 
 * @author prunge
 */
public class NullEntityResolver
implements EntityResolver
{
	@Override
	public InputSource resolveEntity(String publicId, String systemId)
	throws SAXException, IOException
	{
		//Do not resolve entities
		//If we pass back null it uses default which goes to the internet
		//This way it just gets nothing which is what we want
		return(new InputSource(new ByteArrayInputStream(new byte[0])));
	}
}