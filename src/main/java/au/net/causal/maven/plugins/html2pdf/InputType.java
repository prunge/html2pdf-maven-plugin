package au.net.causal.maven.plugins.html2pdf;

/**
 * Controls how input files are parsed.
 * 
 * @author prunge
 */
public enum InputType
{
	/**
	 * Automatically detect XHTML/HTML and pick appropriate mode automatically.
	 */
	AUTO,
	
	/**
	 * Read input as XHTML always.
	 */
	XHTML, 
	
	/**
	 * Read input as HTML always.
	 */
	HTML;
}