package au.net.causal.maven.plugins.html2pdf;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Converts a standard {@link ContentHandler} into a {@link DefaultHandler}.  Methods delegate to the content handler
 * where possible.
 * <p>
 * 
 * If interested, this class can be subclassed to provide feedback for the additional methods in <code>DefaultHandler</code> that 
 * are not in <code>ContentHandler</code>.
 * 
 * @author prunge
 */
public class DefaultContentHandler extends DefaultHandler
{
	private final ContentHandler h;
	
	/**
	 * Creates a <code>DefaultContentHandler</code>.
	 * 
	 * @param h the content handler to wrap.
	 * 
	 * @throws NullPointerException if <code>h</code> is null.
	 */
	public DefaultContentHandler(ContentHandler h)
	{
		if (h == null)
			throw new NullPointerException("h == null");
		
		this.h = h;
	}
	
	@Override
	public void setDocumentLocator(Locator locator)
	{
		h.setDocumentLocator(locator);
	}

	@Override
	public void startDocument() throws SAXException
	{
		h.startDocument();
	}

	@Override
	public void endDocument() throws SAXException
	{
		h.endDocument();
	}

	@Override
	public void startPrefixMapping(String prefix, String uri)
			throws SAXException
	{
		h.startPrefixMapping(prefix, uri);
	}

	@Override
	public void endPrefixMapping(String prefix) throws SAXException
	{
		h.endPrefixMapping(prefix);
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes atts) throws SAXException
	{
		h.startElement(uri, localName, qName, atts);
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException
	{
		h.endElement(uri, localName, qName);
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException
	{
		h.characters(ch, start, length);
	}

	@Override
	public void ignorableWhitespace(char[] ch, int start, int length)
			throws SAXException
	{
		h.ignorableWhitespace(ch, start, length);
	}

	@Override
	public void processingInstruction(String target, String data)
			throws SAXException
	{
		h.processingInstruction(target, data);
	}

	@Override
	public void skippedEntity(String name) throws SAXException
	{
		h.skippedEntity(name);
	}		
}