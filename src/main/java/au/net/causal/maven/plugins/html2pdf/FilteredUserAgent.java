package au.net.causal.maven.plugins.html2pdf;

import org.xhtmlrenderer.extend.UserAgentCallback;
import org.xhtmlrenderer.resource.CSSResource;
import org.xhtmlrenderer.resource.ImageResource;
import org.xhtmlrenderer.resource.XMLResource;

/**
 * User agent that filters all calls through to another.  Designed for subclassing.
 * 
 * @author prunge
 */
public class FilteredUserAgent implements UserAgentCallback
{
	private final UserAgentCallback uac;
	
	public FilteredUserAgent(UserAgentCallback uac)
	{
		if (uac == null)
			throw new NullPointerException("uac == null");
		
		this.uac = uac;
	}
	
	@Override
	public CSSResource getCSSResource(String uri)
	{
		return uac.getCSSResource(uri);
	}

	@Override
	public ImageResource getImageResource(String uri)
	{
		return uac.getImageResource(uri);
	}

	@Override
	public XMLResource getXMLResource(String uri)
	{
		return uac.getXMLResource(uri);
	}

	@Override
	public byte[] getBinaryResource(String uri)
	{
		return uac.getBinaryResource(uri);
	}

	@Override
	public boolean isVisited(String uri)
	{
		return uac.isVisited(uri);
	}

	@Override
	public void setBaseURL(String url)
	{
		uac.setBaseURL(url);
	}

	@Override
	public String getBaseURL()
	{
		return uac.getBaseURL();
	}

	@Override
	public String resolveURI(String uri)
	{
		return uac.resolveURI(uri);
	}
}