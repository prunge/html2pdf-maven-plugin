package au.net.causal.maven.plugins.html2pdf;

public class RelativeUri implements Comparable<RelativeUri>
{
	private final String base;
	private final String uri;
	
	public RelativeUri(String uri, String base)
	{
		this.uri = uri;
		this.base = base;
	}

	public String getBase()
	{
		return(base);
	}

	public String getUri()
	{
		return(uri);
	}
	
	@Override
	public int compareTo(RelativeUri o)
	{
		return(uri.compareTo(o.uri));
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((base == null) ? 0 : base.hashCode());
		result = prime * result + ((uri == null) ? 0 : uri.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RelativeUri other = (RelativeUri) obj;
		if (base == null)
		{
			if (other.base != null)
				return false;
		}
		else if (!base.equals(other.base))
			return false;
		if (uri == null)
		{
			if (other.uri != null)
				return false;
		}
		else if (!uri.equals(other.uri))
			return false;
		return true;
	}
	
	
}